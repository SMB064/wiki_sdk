# How to contribute

The best way to contribute to this Wiki is to `fork` this repo, add your modifications / wiki page and then make a `Merge Request`.
**Please, note that you should never push anything on the `develop` branch otherwise the Mirroring will fail**

## What's the file structure

The wiki is structured like this :
- input : Contain all the input file for the Wiki Generation
    - css : Contain all the .css file used by the Wiki
    - img : Contain all the images used by the Wiki
    - html_parts : Contain all the html parts added to the .md files
    - en : Contain all the English Wiki Pages
    - fr : Contain all the French Wiki Pages (should show the same files minus the credits that are only in english)
    - es : Will contain all the Spanish Wiki Pages
    - index.md : Is the home of the Wiki allowing the User to choose between English & French
    - wiki.json : Contain the Page configuration allowing the renderer to render the correct things
- lib : Contain the Wiki Rendering Engine
- output : Contain the result of `ruby render.rb`
- render.rb : Render the Wiki using `input` as reference

## How to edit a page ?

The Wiki is written in MarkDown, you just need to open the correct .md file and know a few things :
- Image links should always starts with `img/` (unless they are external). They won't show properly but will work in .html result.
- Internal links that starts with 2 letter and a Slash are relative to the input (or ouput) folder. Otherwise the links are always relative to the current file.
- You can show an Image Figure using the following trick : `![alt_text|align](url "Figure caption")` align can be `center`, `left` or `right`. Put the right and left align before the paragraph.
- You can show "tips" using the tag `![tip](class "Message of the tip")`. `class` can be `info`, `warning` or `danger`. Use `danger` only if you want to explicitelly forbit the user to do something.

## How to create a section ?

In this Wiki Engine, a section is a group of article that contain related information. The section help to produce this control in the WikiPage : ![WikiCONTROL](wiki_control.png)

To create a new section you'll have to open `input/wiki.json` and add the following object to the `wiki_sections` collection :
```json
{
  "dir": "name of the section folder, will search .md in input/#{lang}/#{dir}",
  "langs": ["en", "fr"],
  "articles": ["name_of_md1_without_dot_md", "name_of_md2_without_dot_md"]
}
```

The `langs` collection can contain the following languages : 
- `en` for English
- `fr` for French
- `es` for Spanish
- `it` for Italian
- `de` for German
- `ja` for Japanese
- `ko` for Korean

The `articles` collection contain the filename of each article inside `input/#{lang}/#{dir}`. The files should exist for all the languages inside `langs` otherwise the renderer will fail.

## How to create an article ?

To create an article you only need to add the .md file in the `dir` folder for each languages of the section in which this article will be created.

Then you'll write all the translation of the article respecting the following rule : Each article should only have one `h1` (`#`) header. The contents section rely on h2 & more (`##` & more).

Once done, you can save and render the wiki using `ruby render.rb` to check if the page works properly.