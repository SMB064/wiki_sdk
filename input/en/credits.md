# Credits

## Engine related credits

- **SFML** : Moteur graphique utilisé pour le liteRGSS
- **Game Freak, CREATURES INC. et The Pokémon Company** : Univers Pokémon et produits associés
- **Microsoft** : Visual Studio 2012, 2017, Visual Code
- **FIRELIGHT TECHNOLOGIES PTY LTD.** : FMOD lowlevel API
- **Nuri Yuri**
    - Creator of PSDK
    - Programming of FollowMe, Database Engine, Battle Engine & some PSDK modules
    - Programming of RubyFmod
    - Initial contribution in LiteRGSS
- **Aerun**
    - Project Leader of [Forêt Éternelle](https://www.pokemonforeteternelle.com)
    - Pokemon Move Programming
    - Database Editor UI
    - Resources fix
    - Contribution in PSDK Wiki
- **Leikt**
    - Remote game self switches & variables
    - PathFinding Engine
    - Fiber Events (wip)
    - HGSS Slopes
    - Various Fixes
- **Speed / SirMalo**
    - New PSDK UI Design
- **Jayzon** : Gen6 Battle Entry Graphics
- **Otaku** : Alolan Pokemon retrieval & Alpha 24 Battle UI
- **Pαlвσlѕку, Qwerty, Tokeur, Redder, Schneitizel, Mauduss**
    - Gen4 Database
- **Fafa, Jarakaa, Shamoke, BigOrN0t, Bouzouw, Diaband.onceron, Kiros97, Likton, MrSuperluigis, oldu49, SMB64, sylvaintr, UltimaShayra & Unbreakables**
    - Bêta Test of Gen4 PSDK engine
- **Solfay**
    - Old PSDK UI Design
- **Aye, Anti-NT** : Suggestions.
- **Cayrac** : Move Programming
- **FL0RENT_** :Alpha Ruins Puzzle
- **AEliso19**
    - Gen7 Database update (Items & Pokémon)
    - ball_17 (beast ball)
    - ball_18 (GS ball)
- **AEliso19, yyyyj, Maxoumi, Bentoxx, Splifingald, Buttjuice** : Ruby Host Update
- **Neslug** : Shiny Pokémon Animation
- **Renkys** : Quest Book UI
- **Angi-MK, Aethnight & la communauté de Gemme** : Participation in PSDK through communication
- **Helio** : Spanish Translation
- **Eurons** : Trainer Card & KeyBinding UI

## Resource related credits

- **Gen1 to 5 Overworlds** :  
    MissingLukey, help-14, Kymotonian, cSc-A7X, 2and2makes5, Pokegirl4ever, Fernandojl, Silver-Skies, TyranitarDark, Getsuei-H, Kid1513, Milomilotic11, Kyt666, kdiamo11, Chocosrawlooid, Syledude, Gallanty, Gizamimi-Pichu, 2and2makes5, Zyon17, Zenos
- **Gen6 Overworlds** : Princess-Phoenix, LunarDusk6
- **Gen6 Pokémon Battlers** : 
    Diegotoon20, Domino99designs, Falgaia, GeoisEvil, Juan-Amador, N-Kin, Noscium, SirAquaKip, Smogon XY Sprite Project, Zermonious, Zerudez
- **Alolan Sprites** : Smogon
- **Official Pokémon X/Y Texts** : Smogon, X-Act, Peterko, Kaphotics
- **Official Sun & Moon Texts** : Asia81
- **Animated icons** : Pikachumazzinga
- **Gen7 icons** : Poképedia, Otaku
- **Pokémon Offsets** : Don
- **Epic adventures Tileset** :  
    Alistair, Alucus, Bakura155, Bati', Blue Beedrill, BoOmxBiG, Chimcharsfireworkd, CNickC/CNC, CrimsonTakai, Cuddlesthefatcat, Dewitty, EpicDay, Fused, Gigatom, Heavy-Metal-Lover, Hek-el-grande, Kage-No-Sensei, Kizemaru_Kurunosuke, Kymotonian/Kyledove, LaPampa, LotusKing, New-titeuf, Novus, Pokemon_Diamond, PrinceLegendario, Reck, Red-Gyrados, REMY35, Saurav, SL249, Spaceemotion, Speedialga, Stefpoke, sylver1984, ThatsSoWitty, TheEnglishKiwi, Thegreatblaid, ThePokemonChronicles, TwentyOne, UltimoSpriter, Warpras, WesleyFG, Yoh, Youri (Yuri / Nagato Yuki), 07harris/Paranoid, 19dante91, 27alexmad27
- **Gen7 Pokémon Battlers** :  
    Amethyst, Jan, Zumi, Bazaro, Koyo, Smeargletail, Alex, Noscium, Lepagon, N-kin, fishbowlsoul90, princess-phoenix, DatLopunnyTho, Conyjams , kaji atsu , The cynical poet, LuigiPlayer, Falgaia of the Smogon S/M sprite project, Pikafan2000, Lord-Myre
- **PSP Animations**
    - Metaiko (Création des animations)
    - Isomir (Animation Close Combat)
    - KLNOTHINCOMIN (Animation Aéroblast, Eco-Sphere et Fulmigraine)
- **Battle Backs** : Midnitez-REMIX
- **Brick icon & ADN Berserk** : Maxoumi
- **Pink Ribon Icons** : yyyyj
- **Mega-Blastoise Sprites, Cap-Pikachu** : Smogon / Leparagon
- **Meltan & Melmetan** : WolfPP/Wolfang62 et conyjams
- **Mega-Drattak Sprites** : BladeRed et Amras Anarion
- **Default Message windowskin** : ralandel
- **XBOX 360 Keys** : Yumekua

## Special Thanks

PSDK would never have happend without **`Krosk`** the former creator of `Pokémon Script Project` which introduced Nuri Yuri to Pokémon Fangame Making in *2008*.