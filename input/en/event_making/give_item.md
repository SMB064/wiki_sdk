# Give an item

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Give an item from an event
### Command

To give an item from an event you can use the following command :
```ruby
add_item(id, no_delete)
```

The parameters are :
- `id` : ID of the item (# field or db_symbol field in [List of Items](https://psdk.pokemonworkshop.com/db/db_item.html)).
- `no_delete` : If the event shouldn't be permanently deleted. (Deleted by default).

![tip](info "This command shows an "item got" message and plays a music effect if available.")

### Examples

Item on the ground giving a Potion:
```ruby
add_item(17)
```
or
```ruby
add_item(:potion)
```

NPC giving a TM (and that should not be removed) :
```ruby
add_item(328, true)
```
or
```ruby
add_item(:tm01, true)
```

## Silently give an item
### Command

You can add an item to the bag without notification using the following command :
```ruby
$bag.add_item(id, quantity)
```

The parameters are :
- `id` : ID of the item (# field or db_symbol field in [List of Items](https://psdk.pokemonworkshop.com/db/db_item.html)).
- `quantity` : Number of items to add.

### Examples

Add 5 potions :
```ruby
$bag.add_item(17, 5)
```
or
```ruby
$bag.add_item(:potion, 5)
```

## Drop an item from the bag
### Command

To drop an item, use the following command :
```ruby
$bag.drop_item(id, quantity)
```

The parameters are :
- `id` : ID of the item (# field or db_symbol field in [List of items](https://psdk.pokemonworkshop.com/db/db_item.html)).
- `quantity` : Number of items to drop.

### Examples

Drop a potion :
```ruby
$bag.drop_item(17, 1)
```
or
```ruby
$bag.drop_item(:potion, 1)
```

## Savoir si le joueur possède un objet

Pour savoir si le joueur possède un objet en particulier dans son sac, utilisez la Command :
```ruby
$bag.has_item?(id)
```

Le paramètre `id` est l'ID de l'objet qui doit être présent dans le sac.

## Check if the player has an item

To check if the player has an item in the bag, you can use the command :
```ruby
quantite = $bag.item_quantity(id)
```
The parameter `id` is the ID of the item.