# Configure your Project

![tip](warning "This way of configuring a PSDK project will be replaced by .json files in Data")

## The Config Script

In the RMXP Script Editor you'll find a script called `Config`. It contains everything to configure the PSDK project (including some important constant).

![Config|center](img/manage/config.png "The Config Script")

Here's the list of constant you can modify and what they should contain :
- `Title` : It's your game title, this should be a `String`.
- `ScreenWidth` : It's the native screen width, it should be an Integer. The real screen width is this value times the ScreenScale.
- `ScreenHeight` : It's the native screen height, it should be an Integer. The real screen height is this value times the ScreenScale.
- `CenterPlayer` : It's a boolean that allows the player to always be in the center of the screen. Set this value to `false` in order to allow the camera to stop when the player is near the border of the map & the MapLinker is disabled.
- `Pokemon_Max_Level` : It's the default max level (for exp calculation). This should be a number and always be greater or equal than `$pokemon_party.level_max_limit`.  
    ![tip](info "If you want to limit the level of the Player's Pokémon, use `$pokemon_party.level_max_limit = max_level` in a script command in the right events (Starting Event & Gym Leader when they give badges).")
- `DisableMouse` : Set this value to `true` to disable Mouse in your game.

## The viewport configuration

If you remove the `Viewport` module from the `Config` script. PSDK will use `Data/Viewport.json` as source for Viewport configuration.

The file looks like this :
```{
  "main": {
    "x": 0,
    "y": 0,
    "width": 320,
    "height": 240
  }
}```

It's basically a Hash of Hash containing all the viewport configuration. When you write :
```ruby
  @viewport = Viewport.create(:main, z)
```
The x, y, width & height parameters are taken from this file. This way if you want some custom viewport for UI that doesn't take the whole screen, you can use this configurations.

![tip](info "Using `Viewport.create(type, z)` is the only recommanded way to create a viewport. This allow the viewport to be moved when you want to add compatibility to fullscreen with your game thanks to `Viewport::GLOBAL_OFFSET_X` & `Viewport::GLOBAL_OFFSET_Y`. Note that the PSDK screen is 4:3 by default and most computer screens are 16:9 or 16:10.")