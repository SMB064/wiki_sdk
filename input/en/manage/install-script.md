# Install a Script

PSDK has various way to install a script. You can use :
- The RMXP Editor (copy script below Main)
- The `scripts` folder
- `Git`

![tip](danger "There's a `pokemonsdk/scripts` folder. You should never edit the scripts in this folder otherwise you risk to loose your modifications during updates of PSDK.")

## The scripts folder

In the root of your project there's a `scripts` folder. That's where you're supposed to drag & drop your script or create them. 

In this folder, all the scripts folder or script name should start with 5 digits. 

For example :
- A simple script should be called : `scripts/00523 My Script.rb`.
- A script inside a folder should be called : `scripts/01236 Folder Name/03129 My Script.rb`

The order of script loading depends on the digit before the name of the scripts. The folder are always loaded after the script of the same level.

## Using Git
### If your project is not a git repository

If you don't use git for your project, you should use the `git clone` command.

For example, if you want to install the [DynamicLightScript](https://gitlab.com/NuriYuri/dynamiclightscript) you'll perform the following actions :
1. Open `cmd.bat`
2. Execute `cd scripts`
3. Execute `git clone https://gitlab.com/NuriYuri/dynamiclightscript.git "00500 NuriYuri DynamicLight"`  
    ![tip](info "We've set a name to the folder throught the command, you should see the scripts inside `00500 NuriYuri DynamicLight`")
    ![tip](warning "Sometime there's resources to add to your project, check the README.md because for this example, there's graphic resources to download and install!")

### If your project is a git repository

If you use git for your project, the command will be different. We'll use `git submodule add`. This way your project will refer to the script instead of copying the files and not being able to update.

For example, if you want to install the [DynamicLightScript](https://gitlab.com/NuriYuri/dynamiclightscript) you'll perform the following actions :
1. Open `cmd.bat`
2. Execute `cd scripts`
3. Execute `git submodule add https://gitlab.com/NuriYuri/dynamiclightscript.git "00500 NuriYuri DynamicLight"`  
    ![tip](info "We've set a name to the folder throught the command, you should see the scripts inside `00500 NuriYuri DynamicLight`")
    ![tip](warning "Sometime there's resources to add to your project, check the README.md because for this example, there's graphic resources to download and install!")

### Update a script from git

Since you used Git, it'll be easier to update the script, you simply need to move to the script folder and write `git pull`.

Example with the DynamicLightScript :
1. Open `cmd.bat`
2. Execute `cd "scripts/00500 NuriYuri DynamicLight"`
3. Execute `git pull`

Now you're up-to-date!