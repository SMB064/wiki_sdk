# Item editor

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Item editor UI

The `Item` button on the Home UI opens the Item editor, it allows you to define Potions, Balls and a lot of various item.

![Item editor|center](img/ruby_host/Rubyhost_itemsmain.png "Item editor UI")

## Create a new Item

To create a new Item :

1. Make sure all the text required for the new Item exists (Name + Description)
2. Click on the `Add` button.
3. Configure the item according to how you want it to work.
    - If you want the item to be `consumable` check the `Limited use` checkbox.
    - If you don't want the item to be sold set `Price :` to 0.
4. To make an item that affect the Pokémon statistic (HP, Status, EV, PP, etc...) :
    - Click on the `Heal data` button.
    - Edit the data in the window that opens.
    - Don't forget to click on `Save`.  
        ![Heal data editor|center](img/ruby_host/Rubyhost_itemheal.png "Heal data UI")
5. To make a new Ball :
    - Click on the `Catch data` button.
    - Edit the data.
    - The `Specific catch rate (hash) :` field contains information for the catch rate calculation script.
    - Don't forget to click on `Validate`.  
        ![Catch data editor|center](img/ruby_host/Rubyhost_itemcapture.png "Catch data UI")
6. For all other items that does specific things (stone, TM etc...) :
    - Click on the `Specific data` button.
    - Edit the wanted data.
    - Don't forget to click on `Save`.
7. When returning to the Item editor UI don't forget to click on `Save`.

## Create a new TM/HM

To create a new TM/HM :
1. Make sure the item texts exists (Name + Description)  
    ![tip](info "The name of the item should be HM XX or TM XX, the name of the move should not be specified in the name.")
2. Click on `Add`
3. Set the item properties (limited use, map usable etc...)
4. Click on `Specific data`.
    - Check you're editing the new item.
    - Choose the number for `TM ID :` or `HM ID :` (make sure you don't use an ID that is already used).
    - Choose a move in the Skill Learnt drop down
    - Don't forget to save.

![tip](info "`Common Event ID` is used to know which RMXP common event to call when you use the item from the bag.")