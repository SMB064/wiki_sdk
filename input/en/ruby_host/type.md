# Type Editor

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Type editor UI in Ruby Host
Click on `Types` in the Home UI in order to access to this interface.

![Interface|center](img/ruby_host/Rubyhost_typesmain.png "Type editor UI")

The default PSDK types are :
1. Normal
2. Fire
3. Water
4. Electric
5. Grass
6. Ice
7. Fighting
8. Poison
9. Ground
10. Fly
11. Psy
12. Bug
13. Rock
14. Ghost
15. Dragon
16. Steel
17. Dark
18. Fairy