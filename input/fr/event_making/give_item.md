# Donner un objet

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Donner un objet depuis un évènement
### Commande

Pour donner un objet depuis un évènement vous pouvez utiliser la commande :
```ruby
add_item(id, no_delete)
```

Les paramètres sont :
- `id` : ID de l'objet (champ # ou db_symbol de [Liste des objets](https://psdk.pokemonworkshop.com/db/db_item.html)).
- `no_delete` : Indique si l'évènement doit être supprimé définitivement ou non. (Par défaut l'évènement est supprimé).

![tip](info "Cette commande affichera le message d'obtention de l'objet et jouera une musique si possible.")

### Exemples

Objet au sol qui donne une potion :
```ruby
add_item(17)
```
ou
```ruby
add_item(:potion)
```

Personnage qui donne une CT : (Qui ne doit donc pas être supprimé)
```ruby
add_item(328, true)
```
ou
```ruby
add_item(:tm01, true)
```

## Ajouter un objet de manière silencieuse
### Commande

Il est possible d'ajouter un objet dans le sac de manière silencieuse :
```ruby
$bag.add_item(id, quantity)
```

Les paramètres sont :
- `id` : ID de l'objet (champ # ou db_symbol de [Liste des objets](https://psdk.pokemonworkshop.com/db/db_item.html)).
- `quantity` : Le nombre d'objet à donner.

### Exemples

Ajouter 5 potions :
```ruby
$bag.add_item(17, 5)
```
ou
```ruby
$bag.add_item(:potion, 5)
```

## Retirer un objet
### Commande

Pour retirer un objet, il suffit d'utiliser la commande :
```ruby
$bag.drop_item(id, quantity)
```

Les paramètres sont :
- `id` : ID de l'objet (champ # ou db_symbol de [Liste des objets](https://psdk.pokemonworkshop.com/db/db_item.html)).
- `quantity` : Le nombre d'objet à retirer.

### Exemples

Retirer une potion :
```ruby
$bag.drop_item(17, 1)
```
ou
```ruby
$bag.drop_item(:potion, 1)
```

## Savoir si le joueur possède un objet

Pour savoir si le joueur possède un objet en particulier dans son sac, utilisez la commande :
```ruby
$bag.has_item?(id)
```

Le paramètre `id` est l'ID de l'objet qui doit être présent dans le sac.

## Connaitre la quantité d'un objet que le joueur possède

Pour connaître la quantité exacte d'un objet dans le sac utilisez la commande suivante :
```ruby
quantite = $bag.item_quantity(id)
```

Le paramètre `id` est l'ID de l'objet dont vous voulez savoir la quantité.