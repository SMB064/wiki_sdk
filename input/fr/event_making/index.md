# Event Making

Cette section du Wiki contient tous les tutoriels relatif à l'évent Making sous Pokémon SDK. 

Plusieurs informations (parfois très basiques) y seront regroupées.

Vous trouverez notamment comment :
- [Afficher un message](messages.md)
- [Afficher un choix](choices.md)
- [Donner un Pokémon](give_pokemon.md)
- [Donner un objet](give_item.md)
- [Activer le Pokédex](enable_pokedex.md)
- [Donner un badge](give_badge.md)
- [Créer un centre Pokémon](create_pokemon_center.md)
- [Créer un magasin](create_mart.md)
- [Démarrer un combat de Pokémon](start_wild_battle.md)
- [Créer un dresseur](trainer_event.md)
- [Lâcher un pokémon errant](roaming_pokemon.md)
- [Gérer les quêtes](quest.md)
- [Utiliser le FollowMe](followme.md)
- [Modifier les attributs des évènements](event_attribute.md)
- [Système de Temps](time-system.md)