# Gérer les quêtes

![tip](warning "Cette page ne concerne pas le processus de création de quête qui se fait à l'aide de Ruby Host!")

Dans PSDK, la gestion des quêtes se fait au travers de la variable `$quests`. Toute les méthodes sont expliqué dans la documentation : [PFM::Quests](https://psdk.pokemonworkshop.fr/yard/PFM/Quests.html).

Nous allons voir comment utiliser correctement cette variable pour gérer les quêtes.

## 1ère étape : Démarrer une ou plusieurs quêtes

Pour démarrer une ou plusieurs quêtes vous allez appeler un certain nombre de fois : `$quests.start(id)` où `id` est l'id de la quête dans Ruby Host.

Une fois que vous avez appelé `$quests.start(id)` il faut indiquer au système de quête qu'il faut effectivement démarrer les quêtes :
```ruby
$quests.check_up_signal
```

Cette méthode vérifie les quêtes démarrées et terminées puis les valide définitivement. Cette méthode est également censé gérer l'affichage de la nouvelle quête mais ce n'est actuellement pas codé par **manque d'interface**.

## 2ème étape : Valider les objectifs

Il existe un certain nombre de méthodes pour valider les objectifs :
- `$quests.beat_npc(quest_id, npc_name_index)` où `quest_id` est l'id de la quête et `npc_name_index` est l'index du nom du PNJ dans l'ordre de définition des dresseurs à battre dans l'éditeur de quête.
- `$quests.speak_to_npc(quest_id, npc_name_index)` où les paramètres sont les mêmes.

Les autres objectifs sont validés automatiquement par PSDK.

![tip](info "Nous vous conseillons d'appeler $quests.check_up_signal à la fin de chaque évènement qui valident des objectifs.")

## 2ème étape bis : Afficher un objectif caché

Il est possible de cacher les objectifs d'une quête, Ruby Host n'est juste pas capable de permettre de les cacher. Cela dit, si vous trouvez le moyen de définir les objectifs cachés, pour les afficher il faut utiliser la méthode suivante :
```ruby
$quests.show_goal(quest_id, goal_index)
```

Les paramètres sont : 
- `quest_id` est l'id de la quête
- `goal_index` est l'index de l'objectif dans l'ordre des objectifs (s'ils sont tous affichés).

![tip](info "Nous vous conseillons de ne cacher que les objectifs de type battre un dresseur ou parler à un PNJ.")

## 3ème étape : Vérifier la fin d'une quête et distribuer les gains

Pour terminer les quêtes il y a deux choses à faire, vérifier si elle est terminée et distribuer les gains.

![tip](warning "Si vous ne distribuez pas les gains d'une quête le journal de quête ne considèrera pas la quête comme finie.")

### Vérifier si une quête est terminée

Pour vérifier si tous les objectifs ont été réalisés, rien de plus simple, appelez la commande suivante dans une condition :
```ruby
$quests.finished?(quest_id)
```

### Vérifier que les gains ont été distribués

Pour vérifier si les gains ont été distribués, il faut écrire la commande suivante dans une condition :
```ruby
$quests.earnings_got?(quest_id)
```

Si les gains n'ont pas été distribués, distribuez les ainsi :
```ruby
$quests.get_earnings(quest_id)
```

![tip](info "Il est conseillé de vérifier si les gains ont été donnés dans la condition qui a vérifié que la quête est bel est bien terminée!")
