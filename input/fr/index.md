# Accueil

Bienvenue sur le Wiki de Pokémon SDK !

Sur ce Wiki vous trouverez des tutoriels et des informations relatives à PSDK.

## Navigation

### Liens génériques

[Téléchargements](https://download.psdk.pokemonworkshop.com/)
| [Tutoriels d'évent-making](fr/event_making/index.md)
| [Modifier la base de données](fr/ruby_host/index.md)
| [Documentation PSDK](https://psdk.pokemonworkshop.com/yard/)
| [Documentation LiteRGSS](https://psdk.pokemonworkshop.com/litergss/)

### Index de données

[Pokémon](https://psdk.pokemonworkshop.com/db/db_pokemon.html)
| [Talents](https://psdk.pokemonworkshop.com/db/db_ability.html)
| [Objets](https://psdk.pokemonworkshop.com/db/db_item.html)
| [Attaques](https://psdk.pokemonworkshop.com/db/db_skill.html)

## Caractéristiques

Contrairement à `PSP` ou `Essentials`, `PSDK` n'utilise pas le RGSS. Nous avons écrit un moteur Graphique nommé le `LiteRGSS` en se basant sur `SFML`, ceci permet une maîtrise un peu plus grande du moteur graphique.

* Moteur de jeu : `LiteRGSS` (sous `Ruby 2.5.0`)
* Affichage par défaut : `320x240` (upscale à `640x480`)
* Son : [FMOD](http://www.fmod.org/) (Support: Midi, WMA, MP3, OGG, MOD, WAVE)
* Editeur de Map
    * `RMXP`
    * [Tiled](https://pokemonworkshop.fr/forum/index.php?topic=4617.0)
* Editeur d'évènement
    * `RMXP`
    * WIP : VSCODE
* Editeur de données
    * `RubyHost`
* Dépendances : `SFML`, `LodePNG`, `libnsgif`, `FMOD`, `OpenGL`

## Fonctionnalités de PSDK
### Systèmes

- **Temps jour nuit** (`TJN`) utilisant une horloge virtuelle ou réelle.
- **Particules** affichant des animations sur les évènements
- **FollowMe** permettant à un ou plusieurs Pokémon/personnage de suivre le Héros.
- **Journal de quête**
- **Combat doubles et en ligne** (`P2P`)
- **Chaussure de course**
- **Modification des touches de jeu** (Appuyez sur `F1`)
- **Pensions** : avec possibilité d'en avoir plusieurs (comme dans RFVF)
- **Système de baie** : Plantation simple de baies.
- **Echanges en ligne** (`P2P`)
- **GTS** : Vous devez ajouter un script à votre projet.

### Fonctionnalités de Mapping et d'évènement

- **Ombres sous les évènements**
- **Infos étendues** dans le nom des évènements pour permettre certaines choses comme décaller un évènement vers le haut.
- **SystemTags** Tags complémentaire aux terrain tags permettant diverses choses :
    - Info de combat de sauvage (+ Particule) : Haute herbe, grotte, océan, rivière, etc...
    - Tiles du vélo de course (Sol fragile & Pentes glissantes)
    - Tiles du vélo cross (Sauts, Rails blancs)
    - Pentes (Système de HGSS près de puis)
    - Escaliers (Escaliers à la DPP)
    - Ponts (Avec support des évènements)
    - Talus
    - Tiles de rapide et glace (glissant)
    - Sable humide (animation sur le héros)
    - Tiles activant Coud'Boule
- **Dialogues & Textes** utilisant le CSV pour faciliter la traduction
- **Fondus de téléportation** Style 5G et 3G
- **Météo Pokémon** : Pluie, Zénith, Tempête de sable, Neige, Brouillard
- **Evènements communs préconçus** : Force, Tunnel, Vol, Pension, Arbres à Baies, Coud'Boule, Coupe, Cannes, Eclate Roc, Cascade, Tourbillon, Flash, Escalade, Téléport, Anti-Brûme

### Mini-jeux

- Voltorb Flip
- Puzzle Ruine Alpha