# Installer PSDK

Dans cet article nous allons voir comment installer PSDK, si vous n'aimez pas lire, vous pouvez visionner la vidéo de ZeDuckArmy à propos de PSDK (mais l'utilisation de WinRAR est déconseillée) :
![video](https://www.youtube.com/watch?v=ZTJV8N4VSlw "La Vidéo de ZeDuckArmy")

## Télécharger PSDK

Vous avez deux manières de télécharger PSDK. Vous pouvez récupérer l'archive depuis [la page de téléchargement](https://download.psdk.pokemonworkshop.com "La page de téléchargement de PSDK") oui depuis le [Discord Pokémon Workshop ](https://discord.gg/0noB0gBDd91B8pMk).

### Télécharger depuis le Discord Pokémon Workshop

Sur le serveur Discord de Pokémon Workshop, il y a un salon `#access_psdk` où vous pouvez demander l'accès au SDK.

![Access PSDK|center](img/manage/access_psdk.png "Requête anglaise pour l'accès à PSDK")

Une fois que vous aurez obtenu l'accès, quelques salons vont apparaître :
- `#windows` : Vous trouverez le lien de téléchargement dans ce salon.
- `#linux` : Pareil mais pour les utilisateurs de Linux (peu actifs car notre communauté ne dénombre que peu de Linuxiens)
- `#git` : Contient toutes les nouveautés du git (Merge Request & push pour le développement), vous pouvez rendre muet ce salon.
- `#ruby-host` : Contient le lien de Ruby Host
- `#ressources` : Contient des archives qui pourraient vous intéresser (Sprites Pokémon Décompilés, Battlers X/Y & ROSA etc...)

## Installer PSDK

![tip](danger "Vous pourriez être tentés d'extraire l'archive avec `WinRaR`. Vous ne devriez pas. `WinRaR` et `7zip` sont deux logiciels **concurrents**, l'un est payant et l'autre est libre et gratuit et ils ne comprennent pas le format de leur concurrent. Ce qui signifie que vous aurez besoin de [7zip](https://www.7-zip.org) pour extraire PSDK sinon vous risquez d'obtenir des bugs.")

Extrayez l'archive PSDK dans un dossier qui répond aux critères suivants :
- L e dossier est **écrivable par tous** : Cela signifie que PSDK peut se mettre à jour lui-même lorsque vous lui demandez.
- Le chemin menant au dossier ne doit contenir que les caractères suivants : `abcdefghijklmnopqrstuvwxyz _/.\0123456789` (tout autre caractère engendrera des problèmes au lancement de PSDK, les caractères en majuscule fonctionnent).

Exemples :
- C:/PSDK : **fonctionne**
- C:/Windows/System32/PSDK : **Ne** fonctionne **pas**  (non-écrivable)
- C:/Users/my_user_name/Desktop/PSDK : **fonctionne**
- C:/Users/mÿ_ûsèr_ñame/Desktop/PSDK : **Ne** fonctionne **pas** (mauvais caractères)

### Tester si PSDK fonctionne

Pour tester si PSDK fonctionne, suivez les instructions suivantes :
1. Ouvrez `cmd.bat`
2. Écrivez `game`
3. Pressez Entrée

Si la fenêtre de jeu s'ouvre et que le jeu semble fonctionner correctement, vous pouvez fermer la fenêtre.  
Si la fenêtre de jeu ne s'ouvre pas, rapportez le log sur [le forum de Pokémon Workshop](https://pokemonworkshop.fr/forum/).

### Mettre à jour PSDK

La plupart du temps, les archives complètes ne sont pas à jour. Vous devez donc utiliser le Launcher.
1. Ouvrez Launcher.exe
2. Cliquez sur `Détecter et appliquer`

Si la mise-à-jour ne fonctionne pas, essayez la procédure suivante :
1. Ouvrez `cmd.bat`
2. Écrivez `game --util=update`
3. Pressez Entrée

Vous pourrez copier-coller le log sur [le forum de Pokémon Workshop](https://pokemonworkshop.fr/forum/).

## Editer les évènements et les cartes

Pour éditer les évènements et les cartes, vous pouvez ouvrir l'un des fichiers suivants avec RPG Maker XP :
- `Game_RMXP_1.02.rxproj`
- `Game_RMXP_1.03.rxproj`
- `Game_RMXP_1.05.rxproj`

Ils fonctionnent selon votre version de RPG Maker XP .

![tip](info "Si vous ne voyez pas les extensions .rxproj, vous devriez suivre [ce tutoriel](https://www.thewindowsclub.com/show-file-extensions-in-windows). Il est vraiment important de savoir sur quoi l'on clique sinon vous pourriez lancer un fichier `exe`, `com`, `bat` ou `sys` sans le savoir et déclencher un Virus.")

![tip](info "En parlant de Virus, si vous analysez l'archive de PSDK, certains antivirus détectent un trojan, surtout les AV chinois. Si aucun de F-Secure, Kaspersky, MalewareByte, BitDefender, Windows Defender ou MacAfee ne détecte de virus, vous pouvez considéré que l'archive est hors de soupçons. En cas de doute, téléchargez toujours l'archive depuis des sources officielles.")

