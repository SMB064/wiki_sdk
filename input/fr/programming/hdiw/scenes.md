# Scènes (GamePlay)

PSDK a été écrit depuis la base RMXP. Ce qui signifie que le protocle d'une scène basique fonctionne : faire une classe dont les objets obéissent à `main`. Avec l'évolution de PSDK, nous avons créé une classe `GamePlay::Base` et commencé à faire hériter toutes les scènes depuis celle-ci. Le but de `GamePlay::Base` est de gérer plusieurs aspects d'une scène :
- Appeler la boucle update
- Gérer les messages (`display_message`)
- Appeler d'autres scènes
- Appeler la méthode `dispose`

Aujourd'hui nous utilisons `GamePlay::BaseCleanUpdate` qui dérive de `GamePlay::Base` et nous avons seulement besoin de définir les méthodes suivantes :
- `initialize`
- `create_graphics`
- `update_inputs`

Le reste est géré ou optionnel. Pour expliquer certaines méthodes, nous allons expliquer le cycle de fonctionnement d'un objet enfant de `GamePlay::BaseCleanUpdate`.

## Cycle de fonctionnement

Ceci est le diagramme simplifié du cycle de fonctionnement :
![diagram|center](img/programming/hdiw/gpbcu.png "Diagramme du cycle de fonctionnement")

Les rectangles bleus correspondent aux méthodes que vous ne devriez jamais définir vous-même. La méthode `dispose` va relâcher tous les viewports contenus dans une variable d'instance contenant le mot viewport dans son nom. Vous pouvez réécrire la méthode pour relâcher des bitmaps si vous n'utilisez pas le cache mais n'oubliez pas d'appeler `super` quand vous faites ça pour relâcher tout de même les viewports.

### initialize

La première chose à faire est de définir `initialize`. Cette méthode peut accepter n'importe quel paramètre du moment qu'ils permettent à la scène de fonctionner correctement. La méthode `initialize` **doit** appeler `super()` (cette méthode crée la fenêtre de message et essaie de prévoir une fermeture douce pour les messages qui n'ont pas terminé de s'afficher avant que la scène soit appelée).

Dans la méthode `initialize` , l'état de la scène est la seule chose qui devrait être créée, graphics, viewport etc... ne doivent jamais être créé dans `initialize`.

### create_graphics

La seconde méthode à définir est `create_graphics`, c'est mieux lorsque la scène ressemble à quelque chose. Cette méthode est automatiquement appelée par `GamePlay::Base` , vous n'avez donc pas besoin de l'appeler explicitement.

![tip](warning "Quand vous définissez `create_graphics` n'oubliez pas d'appeler `super` ou `create_viewport`. La méthode `create_graphics` de GamePlay appelle juste `create_viewport`")

### create_viewport

Habituellement, vous n'appellerez jamais `create_viewport` sauf si vous avez besoin de plus d'un viewport. la méthode `create_viewport` par défaut crée le `main` viewport dans la variable `@viewport`. Si vous avez besoin de plus de viewports, utilisez des variables d'instances qui contiennent le mot `viewport`.

![tip](info "Toutes les variables qui contiennent `viewport` dans leur nom recevront l'appel de leur propre `dispose` depuis la méthode `dispose`.")

### update_inputs

Dans les objets enfants de `GamePlay::BaseCleanUpdate`, `update_inputs` est une méthode publique. Elle est appelée automatiquement par `update` s'il n'y a pas de message affiché. Vous gérerez tous les inputs ici et la méthode retournera un booléen pour savoir si les interactions avec la souris doivent être mis à jour.

### update_mouse

Dans les objets enfants de `GamePlay::BaseCleanUpdate`, `update_mouse` est une méthode publique qui reçoit l'argument booléen `moved`. Elle est aussi automatiquement appelée par `update` si aucun message n'est affiché et que `update_inputs` ne retourne pas `false`. Vous gérerez les interactions de la souris ici et la méthode renverra un booléen qui conditionnera la valeur renvoyée par `update`.

### update_graphics

Dans les objets enfants de `GamePlay::BaseCleanUpdate`, `update_graphics` est une méthode publique. Elle est appelée à chaque frame sans tenir compte de l'état de la scène. You pouvez mettre à jour les animations par exemple. Cette méthode va retourner un booléen qui conditionnera le retour de `update`.

## Exemple d'une scène qui ne fait rien

Pour montrer la facilité de définition d'une scène, je mets à votre disposition un script qui montre seulement un nombre au hasard et se ferme quand vous pressez la touche Echap:

```ruby
class MyScene < GamePlay::BaseCleanUpdate
  def initialize
    super()
    @random_number = rand(0xFFFFFFFF)
  end

  def update_inputs
    return @running = false if Input.trigger?(:B)
    return true
  end

  private

  def create_graphics
    super # Crée la variable @viewport
    create_text
  end

  def create_text
    @text = Text.new(0, @viewport, 0, 0 - Text::Util::FOY, 320, 16, @random_number.to_s, 1)
  end
end
```

## Comment appeler une autre scène

Pour appeler une autre scène, vous aurez besoin d'appeler `call_scene(Scene, *args)`.
- `Scene` est la class de la scène à appeler.
- `*args` correspond aux arguments à passer à `Scene.new`.

La méthode `call_scene` accepte un block avec le paramètre `scene` (qui est l'instance de la scène appelée). Ce block est appelé une fois que la scène a été disposée et il est très utile pour récupérer ce que retourne la scène appelée (comme le Pokémon choisi).

Exemple : 
```ruby
call_scene(GamePlay::Save) do |scene|
  @running = false if scene.saved
end
```
La méthode appelera `GamePlay::Save` sans arguments et arrêtera la scène actuelle lorsque le joueur aura sauvegardé. (selon la documentation de GamePlay::Save).

## Comment retourner à Scene_Map
Certaines scènes de Menu souhaiteraient revenir à `Scene_Map` (pour plusieurs raisons) ? Si toutes les scènes précédentes appellent `call_scene` le meilleur moyen de retourner `Scene_Map` est d'appeler `return_to_scene(Scene_Map)`. 

Quand `call_scene` est utilisé par la scène précédente, si `return_to_scene` est utilisé par la scène actuelle, la transition vers la première ne sera pas réalisée sauf s'il s'agit de `Scene_Map`. (Cela permet de passer différentes transitions).

![tip](info "Quand `@running` est mis à false par le paramètre du block `call_scene`, la transition vers la scène actuelle n'est pas non plus effectuée.")