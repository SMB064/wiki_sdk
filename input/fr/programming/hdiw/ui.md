# Le Système d'UI de PSDK

Dans PSDK les UI sont créées dans le module `UI`. La plupart des parties d'une UI héritent de la classe `SpriteStack` et acceptent une entrée `data` qui aide à dire à chaque composant ce qu'il doit afficher.

## Les différents objets d'UI
### SpriteStack

Le SpriteStack est un objet conceptuel qui range tous les petits éléments de l'UI dans un Array. Cela permet de faire la chose suivante :
- Définir des sprites dans un sous-espace dont l'origine (0, 0) est la coordonnée de la "stack" (ou "pile"). Cela permet de ne définir la stack qu'une seule fois et de l'afficher à différentes coordonnées sur l'écran. (Les sprites se déplaceront de la même façon)
- Déplacer tous les sprites dans la stack à d'autres coordonnées
- Remplir le paramètre `data=` avec un objet comme un  Pokémon, le dresseur ou d'autres choses.
- Mettre à jour tous les sprites de la stack (si c'est nécessaire)
- Optionnellement, vous pouvez aussi relâcher tous les sprites dans la stack mais ceci est considéré comme une mauvaise pratique.

#### initialize

Actuellement, la méthode initialize prend trois arguments de position et 1 argument "mot-clé" :
- `viewport` : Le viewport dans lequel le SpriteStack doit apparaître
- `x` : X coordonnée de l'origine de la stack (optionnel)
- `y` : Y coordonnée de l'origine de la stack (optionnel)
- `default_cache:` : Nom du cache utilisé par les images de la stack (par défaut : `:interface`).

#### add_background

Cette méthode permet d'afficher l'arrière-plan de la stack (à la coordonnée d'origine). Elle prend les arguments suivants :
- `filename` : Nom de l'image à utiliser comme arrière-plan (cherché dans le cache par défaut).
- `type:` : Classe utilisée pour afficher l'arrière-plan (par défaut : Sprite)
- `rect:` : Rect utilisé pour afficher l'arrière-plan (si vous ne voulez pas utiliser la surface entière, optionnel)

#### add_sprite

Cette méthode permet d'afficher un sprite avec différents paramètres. Elle prend les arguments suivants :
- `x` : X coordonnée du sprite dans la stack
- `y` : Y coordonnée du sprite dans la stack
- `bmp` : Nom de l'image à afficher (peut être nil si vous ne voulez pas afficher d'image ou utilisé un Sprite spécifique)
- `*args` : Arguments qui viennent après `viewport` dans l'appel de `type.new`
- `rect:` : Rect utilisé pour afficher l'arrière-plan (si vous ne voulez pas utiliser la surface entière, optionnel)
- `type:` : Classe utilisée pour afficher le sprite (par défaut : Sprite)
- `ox:` : ox du sprite (par défaut : 0)
- `oy:` : oy du sprite (par défaut : 0)

#### add_text

Cette méthode permet d'ajouter du texte à la stack, elle prend les arguments suivants :
- `x` : X coordonnée du texte dans l'espace de la stack
- `y` : Y coordonnée du texte dans l'espace de la stack
- `width` : Largeur utilisée pour la surface du texte (utile pour l'alignement à gauche)
- `height` : Hauteur d'une ligne de texte
- `str` : Texte à afficher ou Symbol de la méthode à appeler pour récupérer un texte (SymText)
- `align` : Alignement horizontal du texte dans la zone (par défaut : 0)
- `outlinesize` : Taille de l'ombre (par défaut : nil => shadow)
- `type:` : Classe utilisée pour afficher le texte (par défaut : Text)
- `color:` : Couleur utilisée pour afficher le texte (pareil que les messages)
- `size_id:` : Taille à utiliser pour afficher le texte (par défaut : nil)

Exemple d'utilisation :
```ruby
  add_text(0, 0, 320, 16, "Pokémon:", color: 10) # Will show Pokemon on the first line in white
  add_text(0, 16, 320, 16, :name, type: UI::SymText) # Will show the name of the Pokemon
```

#### with_cache

Cette méthode vous permet de définir des sprites en utilisant une autre source de cache que celle par défaut. Elle prend un paramètre qui est le nom du cache. Exemple :

```ruby
  def initialize(viewport)
    super(viewport, default_cache: :interface)
    with_cache(:picture) do
        add_background('GTS')
    end
    add_sprite(25, 32, 'something_in_interface')
  end  
```

#### with_font

Cette méthode est un peu comme `with_cache` mais pour le texte. Elle permet d'utiliser une autre police que `0` pour un texte. Dans PSDK la police par défaut (0) a la taille de 13px, la police alternative (1) a une taille de 26px et la police minuscule (20) vous permet d'afficher plus de texte dans un même espace horizontal (utilisées pour afficher les noms des Pokémons sur les barres de combat). Exemple :
```ruby
  def initialize(viewport)
    super
    with_font(1) do
      add_text(0, 0, 320, 32, "Big Text", 0, 0)
    end
    with_font(20) do
      add_text(0, 32, 320, 10, "Small Text line 1", 0, 1)
      add_text(0, 42, 320, 10, "Small Text line 2", 0, 1)
    end
    add_text(0, 64, 320, 16, "Regular Text")
  end
```

#### set_position

Cette méthode est la même que celle des sprites, elle assigne aux coordonnées x & y de la SpriteStack celles que l'on entre en paramètres.

Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport, 5, 5)
  # ...
  stack.set_position(10, 20) # stack.x = 10, stack.y = 20
```

#### move

Cette méthode vous permet de déplacer toute la stack par rapport à sa position actuelle.

Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport, 10, 20)
  # ...
  stack.move(5, 5) # stack.x = 15, stack.y = 25
```

#### simple\_mouse\_in?

Cette méthode permet de vérifier si la souris est sur le premier sprite de la stack (habituellement l'arrière-plan). Cela aide à faire une UI interactive.

Exemple : 
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_background('button')
  # ...
  if stack.simple_mouse_in?
    p "User mouse is inside the button"
  end
```

### Type1Sprite

Cette classe vous permet d'afficher le 1er type d'un Pokémon. Il prend un paramètre booléen optionnel qui dit si le type1 est montré depuis le Pokédex ou pas (les affichages graphiques sont différents).

Exemple : 
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::Type1Sprite)
  # ...
  stack.data = $actors[0] # Shows the type 1 of the first Pokémon in the party
```

### Type2Sprite

Cette classe vous permet d'afficher le 2ème type d'un Pokémon. Il prend un paramètre booléen optionnel qui dit si le type2 est montré depuis le Pokédex ou pas (les affichages graphiques sont différents).

Exemple : 
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, true, type: UI::Type2Sprite)
  # ...
  stack.data = $actors[0] # Shows the type 2 (Pokédex type image) of the first Pokémon in the party
```

![tip](info "Rappelez-vous que les arguments envoyés après le nom de l'image (`nil` ici) sont envoyés à l'`initialize` du Sprite (`UI::Type2Sprite` ici). Dans ce cas nous avons envoyé true qui dit d'utiliser l'image du Pokédex.")

### TypeSprite

Cette classe vous permet d'afficher le Type d'une capacité ou n'importe quel objet possédant une méthode `type` et qui retourne un Integer.

Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::TypeSprite)
  # ...
  stack.data = $actors[0].skills_set[0] # Show the type of the first move of the first Pokémon in the party
```

### GenderSprite

Affiche le genre d'un Pokémon. Exemple : 

```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::GenderSprite)
  # ...
  stack.data = $actors[0] # Show the gender of the first Pokemon in the party
```

### StatusSprite

Affiche le statut d'un Pokémon. Exemple : 

```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::StatusSprite)
  # ...
  stack.data = $actors[0] # Show the status of the first Pokemon in the party
```

### HoldSprite

Montre l'icône d'objet porté par un Pokémon (s'il porte un objet). Exemple : 

```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::HoldSprite)
  # ...
  stack.data = $actors[0] # Show the hold indicator of the first Pokemon in the party
```

### RealHoldSprite

Montre l'icône de l'objet tenu par un Pokémon. Exemple :

```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::RealHoldSprite)
  # ...
  stack.data = $actors[0] # Show the held item of the first Pokemon in the party
```

### CategorySprite

Affiche la catégorie d'une capacité (physique, spéciale, de statut). Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::CategorySprite)
  # ...
  stack.data = $actors[0].skills_set[0] # Show the type of the first move of the first Pokémon in the party
```

### PokemonFaceSprite

Montre le sprite de face d'un Pokémon. Cet objet accepte un booléen (qui est mis à true par défaut) qui dit si le Sprite devrait être aligné au centre à partir du bas de l'image. (Ce qui signifie que les coordonnées du sprite sont ceux des pieds du Pokémon)..

Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::PokemonFaceSprite)
  # ...
  stack.data = $actors[0] # Show the face sprite of the first Pokemon in the party
```

![tip](info "Ce sprite tentera de charger le fichier gif si celui-ci existe.")

### PokemonBackSprite

Montre le sprite de dos d'un Pokémon. Cet objet accepte un booléen (qui est mis à true par défaut) qui dit si le Sprite devrait être aligné au centre à partir du bas de l'image. (Ce qui signifie que les coordonnées du sprite sont ceux des pieds du Pokémon).

Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::PokemonBackSprite)
  # ...
  stack.data = $actors[0] # Show the back sprite of the first Pokemon in the party
```

![tip](info "Ce sprite tentera de charger le fichier gif si celui-ci existe.")

### PokemonIconSprite

Affiche l'icône d'un Pokémon. Cet objet accepte un booléen (qui est `true` par défaut) qui demande au sprite d'être centré.

Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::PokemonIconSprite)
  # ...
  stack.data = $actors[0] # Show the icon sprite of the first Pokemon in the party
```

### PokemonFootSprite

Montre les traces de pas d'un Pokémon. Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_sprite(0, 0, nil, type: UI::PokemonFootSprite)
  # ...
  stack.data = $actors[0] # Show the foot print of the first Pokemon in the party
```

### SymText

Affiche un texte en utilisant une méthode de l'objet contenu dans data comme source de texte. Exemple : 
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_text(0, 0, 320, 16, :given_nale, type: UI::SymText)
  # ...
  stack.data = $actors[0] # Show the nickname of the first Pokemon in the party
```

### SymMultilineText

Affiche un texte multi-lignes en utilisant une méthode de l'objet contenu dans data comme source de texte. Exemple :
```ruby
  stack = UI::SpriteStack.new(viewport)
  stack.add_text(0, 0, 320, 16, :description, type: UI::SymMultilineText)
  # ...
  stack.data = $actors[0].skills_set[0] # Show the description of the first move of the first Pokemon in the party
```

## Informations importantes

Dans l'exemple, nous avons beaucoup vu un unique ajout d'élément d'UI pouvant être modifié dynamiquement selon la `data` envoyée au sprite stack. **Vous pouvez ajouter plusieurs éléments d'UI qui changent selon la `data` dans la même stack**.

Si vous voulez aller plus loin et afficher Pokemon Info et l'info des 4 capacités, vous pouvez ajouter une sous-stack à la stack et modifier la méthode `data=` de la stack Pokemon Info pour envoyer la bonne information à la stack fille. Exemple : 
```ruby
class PokemonInfo < UI::SpriteStack
  def initialize(viewport)
    super
    # ...
    @moves = Array.new(4) do |i|
      PokemonMoveInfo.new(viewport, i)
    end
  end

  def data=(pokemon)
    super(pokemon) # Update the sprites of the PokemonInfoStack
    @moves.each_with_index do |move, i|
      move.data = pokemon&.skills_set&.at(i)
    end
  end

  # Ensure the moves can move the this stack
  def move(delta_x, delta_y)
    super
    @moves.each { |move| move.move(delta_x, delta_y) }
  end
end
```

![tip](warning "Dans ce cas vous ne pourrez pas ajouter de PokemonMoveInfo comme custom_sprites car ils répondent à `data=` et auraient reçu un Pokémon au lieu d'une capacité.")