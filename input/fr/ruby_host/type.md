# Editeur de Type

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Interface des Types de RubyHost
Le bouton `Types` accessible sur l'interface d'accueil de RubyHost permet d'accéder à une fenêtre permettant de modifier les rapports offensifs et défensifs entre les types, ainsi que de créer un nouveau type.

![Interface|center](img/ruby_host/Rubyhost_typesmain.png "Interface d'édition des rapports entre les types")

Par défaut, PSDK propose les types suivant :
1. Normal
2. Feu
3. Eau
4. Électrik
5. Plante
6. Glace
7. Combat
8. Poison
9. Sol
10. Vol
11. Psy
12. Insecte
13. Roche
14. Spectre
15. Dragon
16. Acier
17. Ténèbres
18. Fée